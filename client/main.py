import json
from random import randrange
import requests

url_app = "http://127.0.0.1:8000/random/"


def read_rudy_file(path):
    with open(path, "r") as file:
        rudy = json.load(file)
    return rudy


def generate_random_playlist(artists: list, playlist_length: int) -> list:
    playlist = []
    while playlist_length > 0:
        select_random_artist = randrange(0, len(artists))
        r = requests.get(
            url_app + artists[select_random_artist]["artist"])
        if r.status_code == 200:
            playlist.append(r.json())
        playlist_length -= 1
    return playlist


def generate_rudy_playlist(playlist_length: int) -> list:
    playlist = []
    rudy_artists = read_rudy_file("rudy.json")
    while playlist_length > 0:
        select_random_artist = randrange(0, len(rudy_artists))
        # select only artist that Rudy graded 10 or more
        while rudy_artists[select_random_artist]["note"] < 10:
            select_random_artist = randrange(0, len(rudy_artists))
        r = requests.get(
            url_app + rudy_artists[select_random_artist]["artiste"])
        if r.status_code == 200:
            playlist.append(r.json())
        playlist_length -= 1
    return playlist
