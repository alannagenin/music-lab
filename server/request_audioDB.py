import requests
from random import randint

audioDB_url = "https://www.theaudiodb.com/api/v1/json/2/"


def healthcheck_audioBD() -> bool:
    r = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php")
    if r.status_code == 200:
        return True
    else:
        return False


def get_artist_id_from_name(artist_name: str) -> str:
    payload = {"s": artist_name}
    r = requests.get(audioDB_url + "/search.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        return r["artists"][0]["idArtist"]
    else:
        raise ConnectionError


def get_random_album_from_artist_id(artist_id: int) -> str:
    payload = {"i": artist_id}
    r = requests.get(audioDB_url + "/album.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        num_album = randint(0, len(r["album"]))
        return r["album"][num_album]["idAlbum"]
    else:
        raise ConnectionError


def get_random_track_id_from_album_id(album_id: int) -> str:
    payload = {"m": album_id}
    r = requests.get(audioDB_url + "/track.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        num_track = randint(0, len(r["track"]))
        return r["track"][num_track]["idTrack"]
    else:
        raise ConnectionError


def get_random_track_name_from_album_id(album_id: int) -> str:
    payload = {"m": album_id}
    r = requests.get(audioDB_url + "/track.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        num_track = randint(0, len(r["track"]))
        return r["track"][num_track]["strTrack"]
    else:
        raise ConnectionError


def get_random_track_id_from_artist_id(artist_id: int) -> str:
    payload = {"i": artist_id}
    r = requests.get(audioDB_url + "/mvid.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        num_track = randint(0, len(r["mvids"]))
        return r["mvids"][num_track]["idTrack"]
    else:
        raise ConnectionError


def get_random_track_video_from_artist_id(artist_id: int) -> str:
    payload = {"i": artist_id}
    r = requests.get(audioDB_url + "/mvid.php",
                     params=payload)
    if r.status_code == 200:
        r = r.json()
        num_track = randint(0, len(r["mvids"]))
        return {"title": r["mvids"][num_track]["strTrack"],
                "suggested_youtube_url": r["mvids"][num_track]["strMusicVid"]}
    else:
        raise ConnectionError
