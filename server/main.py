import request_audioDB as songs
import request_lyricsOvh as lyrics
from fastapi import FastAPI
from fastapi_health import health


def healthy_condition():
    return {"database": "online"}


def sick_condition():
    return False


app = FastAPI()
app.add_api_route("/health", health([healthy_condition, sick_condition]))


@app.get("/")
def read_root():
    return {"endpoints": ["random, health"]}


@app.get("/random/{artist_name}")
def get_random_song_from_artist_name(artist_name: str):
    artist_id = songs.get_artist_id_from_name(artist_name)
    track_info = songs.get_random_track_video_from_artist_id(artist_id)
    title_track = track_info["title"]
    lyrics_track = lyrics.get_lyrics_from_artist_and_title(
        artist_name, title_track
    )
    return {
        "artist": artist_name,
        "title": title_track,
        "suggested_youtube_url": track_info["suggested_youtube_url"],
        "lyrics": lyrics_track
    }
