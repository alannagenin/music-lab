import requests


def healthcheck_lyricsOvh() -> bool:
    r = requests.get("https://api.lyrics.ovh/v1/Kodaline/Brother")
    if r.status_code == 200:
        return True
    else:
        return False


def get_lyrics_from_artist_and_title(artist: str, title: str) -> str:
    r = requests.get("https://api.lyrics.ovh/v1/" + artist + "/" + title)
    if r.status_code == 200:
        r = r.json()
        return r
    elif r.status_code == 404:
        raise KeyError
    else:
        raise ConnectionError


def get_status_code(artist: str, title: str) -> str:
    r = requests.get("https://api.lyrics.ovh/v1/" + artist + "/" + title)
    if r.status_code == 200:
        return r.status_code
    else:
        raise ConnectionError
