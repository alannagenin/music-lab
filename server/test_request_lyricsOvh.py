import unittest
import pytest
# from unittest import mock
from request_lyricsOvh import get_lyrics_from_artist_and_title
from request_lyricsOvh import get_status_code
from request_lyricsOvh import healthcheck_lyricsOvh


def mocked_requests_get(*args, **kwargs):
    '''This method will be used by the mock to replace requests.get'''
    class MockResponse:
        def __init__(self, status_code, json_data):
            self.status_code = status_code
            self.json_data = json_data

        def json(self):
            return self.json_data

    if args[0] == "https://api.lyrics.ovh/v1/Kodaline/Brother":
        return MockResponse(
            status_code=200,
            json_data={
                "lyrics": "When we were young We were the ones"}
        )
    elif args[0] == "https://api.lyrics.ovh/v1/Kodaline/Sister":
        return MockResponse(
            status_code=404,
            json_data={"error": "No lyrics found"}
        )

    return MockResponse(status_code=404, json_data=None)


def test_healthcheck_lyricsOvh():
    check = healthcheck_lyricsOvh()
    assert check is True


@unittest.mock.patch('requests.get', side_effect=mocked_requests_get)
def test_get_lyrics_from_artist_and_title_passed():
    lyrics = get_lyrics_from_artist_and_title("Kodaline", "Brother")
    assert lyrics == "When we were young We were the ones"


@unittest.mock.patch('requests.get', side_effect=mocked_requests_get)
def test_get_status_code_from_artist_and_title_passed():
    status_code = get_status_code("Kodaline", "Brother")
    assert status_code == 200


@unittest.mock.patch('requests.get', side_effect=mocked_requests_get)
def test_get_lyrics_from_artist_and_title_fail():
    with pytest.raises(KeyError):
        get_lyrics_from_artist_and_title("Kodaline", "Sister")


@unittest.mock.patch('requests.get', side_effect=mocked_requests_get)
def test_get_status_code_from_artist_and_title_fail():
    status_code = get_status_code("Kodaline", "Sister")
    assert status_code == 404


if __name__ == '__main__':
    unittest.main()
