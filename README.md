# music-lab

Réalisation d'une architecture client-serveur pour la création de playlists

## Présentation

Cette API a pour but de proposer une playlist à partir des goûts musicaux de l'utilisateurs. Pour ce faire, deux API ont été utilisées : [AudioDB](https://www.theaudiodb.com/) et [Lyrics OVH](https://lyricsovh.docs.apiary.io/).

## Quick start

Pour installer l'application, il suffit de lancer les commandes suivantes dans un terminal.

```
git clone git@gitlab.com:alannagenin/music-lab.git
cd music-lab
```

## Utilisation de l'application

### Côté serveur

```
cd serveur
pip install -r requirements.txt
uvicorn main:app
```

Exemple

```
# Indochine
http://127.0.0.1:8000/random/Indochine
# The Script (nécessité d'échapper les espaces)
http://127.0.0.1:8000/random/The%20Script
```

### Côté client

Pour que le client fonctionnne, il faut au préalable que le serveur soit lancé.

```
cd ../client
pip install -r requirements.txt
uvicorn main:app
```

### Endpoints

Trois endpoints sont disponibles, il s'agit des points d'accès **/random/{artist name}**, **/health** et **/** . Pour y avoir accès, il faut faire une requête <em>GET</em> via le protocole <em>HTTP</em>.

1. **/random/{artist_name}** choisit aléatoirement une chanson de l'artiste (spécifié via <em>artist_name</em>) avec les informations suivantes : nom de l'artiste, titre de la chanson, lien de la vidéo YouTube avec les paroles
2. **/health** effectue un <em>healthcheck</em> c'est-à-dire vérifie que l'API fonctionne bien
3. **/** affiche tous les endpoints disponibles (<em>i.e. random, health </em>)

## Développement de l'application

### Schéma d'architecture

Cette application requête deux API : d'une part [AudioDB](https://www.theaudiodb.com/) qui permet d'avoir accès aux informations des artistes, leurs albums, le contenu des albums, etc. et d'autre part [Lyrics OVH](https://lyricsovh.docs.apiary.io/) qui, à partir d'un nom d'artiste et d'une chanson, renvoit les paroles.

```mermaid
graph TD
    ClientPy[Client music-lab] -->|http request| ServerPy[Server music-lab]
    ServerPy --> |json file| ClientPy
    ServerPy --> |http request| AuDB[AudioDB API]
    AuDB --> |json file| ServerPy
    ServerPy --> |http request| LyOVH[Lyrics OVH API]
    LyOVH --> |json file| ServerPy
    ClientPy --> |json file| Host[Host]
    Host --> |json file| ClientPy
```

### Tests unitaires

Enfin, ce projet comprend des tests unitaires qui s'intègrent de façon continue. Si l'utilisateur souhaite les exécuter, il suffit de lancer la commande suivante dans un terminal.

```
pytest
```
